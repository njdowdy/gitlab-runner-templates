# gitlab-runner-templates

Code for deploying gitlab runners

## Getting started

Go to [Digital Ocean](https://cloud.digitalocean.com/droplets/new?i=d50dca&size=c-2-4gib&region=nyc1&appId=87786318&image=docker-20-04&type=applications&options=install_agent) and create a droplet running Docker on Ubuntu. Name your droplet something easy to remember like "gitlab-runner-host-00".

## Installation

1. SSH into droplet
2. `cd /home`
3. `git clone https://gitlab.com/njdowdy/gitlab-runner-templates.git`
4. `cd gitlab-runner-templates`
5. `apt install make`
6. `sudo cp example.env .env`
7. `sudo nano .env`
8. Setup environment variables (registration token from project needing runners)
9. `sudo make dc-init-linux`
10. Profit!

## Special Note

Running a Digital Ocean droplet, even if turned off, costs money!

When not actively in use, be sure to take a snapshot of your droplet and then Destroy the droplet to avoid being charged during periods of inactivity.

## Authors and acknowledgment

Nicolas J. Dowdy

Inspiration from this [post](https://testdriven.io/blog/gitlab-ci-docker/)

## License

TBD

## Project status

Interested in expanding utility to other service platforms and operating systems.
