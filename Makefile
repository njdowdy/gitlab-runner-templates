include .env

.PHONY: help dc-setup dc-init-linux dc-clean dc-stop dc-start dc-start-verbose dc-build

SHELL := /bin/bash ## set shell to bash to use source in .sh scripts

help: ## Show this help menu
	@echo "Usage: make [TARGET ...]"
	@echo ""
	@grep --no-filename -E '^[a-zA-Z_%-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "%-15s %s\n", $$1, $$2}'

dc-setup: ## setup docker-compose
	@mkdir -p ~/.docker/cli-plugins/;
	@curl -SL https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
	@chmod +x ~/.docker/cli-plugins/docker-compose

dc-start: dc-stop dc-build ## Start docker (might need sudo)
	@docker-compose up -d;

dc-start-verbose: dc-stop dc-build ## Start docker with messages
	@docker-compose up;

dc-stop: ## Stop docker (might need sudo)
	@docker-compose stop;

dc-build:
	@docker-compose build;

dc-init-linux: dc-setup dc-start  ## Initialize and create runner container
	@chmod +x ./linux_register_runners.sh
	@./linux_register_runners.sh