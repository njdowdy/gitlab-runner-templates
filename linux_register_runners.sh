# runs in context of docker host (droplet)
source ./.env
# register runners
for i in $(seq ${NUMBER_OF_RUNNERS}); do \
    docker-compose exec gitlab-runner-remote-linux \
    gitlab-runner register \
    --non-interactive \
    --url ${GITLAB_URL} \
    --registration-token ${GITLAB_REGISTRATION_TOKEN} \
    --executor ${RUNNER_EXECUTOR_TYPE} \
    --description "Runner_$i" \
    --docker-image "docker:${DOCKER_VERSION}" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --tag-list "${RUNNER_TAGS},runner_$i" \
    --maximum-timeout ${MAX_TIMEOUT}; done

# configure runners
sed -e 's/concurrent = [0-9]/concurrent = '${CONCURRENT_PROCS_PER_RUNNER}'/g' -i ./linux/config/config.toml
sed -i '/.cache./d' ./linux/config/config.toml
if [ "${USE_RUNNER_CACHE}" = "True" ] ; then
    sed -i '/^  \[runners\.custom\.*/i \ \ cache_dir = \"'${CACHE_DIR}'\"' ./linux/config/config.toml &&     
    sed -i '/^    disable_entrypoint_overwrite\.*/i \ \ \ \ volumes = \[\"'${CACHE_DIR}'\"\]' ./linux/config/config.toml &&
    sed -i '/^    disable_entrypoint_overwrite\.*/i \ \ \ \ disable_cache = false' ./linux/config/config.toml && 
    sed -i '/^    disable_entrypoint_overwrite\.*/i \ \ \ \ cache_dir = \"\"' ./linux/config/config.toml ; else 
    echo "Skipping cache setup..."; fi
sed -i '/^  \[runners\.custom\.*/i \ \ limit = '${CONCURRENT_JOB_LIMIT}'\n  request_concurrency = '${CONCURRENT_PROCS_PER_RUNNER}'' ./linux/config/config.toml
sed -e 's/privileged = false/privileged = true/g' -i ./linux/config/config.toml
# restart runner
docker-compose restart gitlab-runner-remote-linux
# setup weekly CRON job to clean up unused images and containers
crontab ./crontab.txt
